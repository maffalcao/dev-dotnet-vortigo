﻿using Humanizer.Bytes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Portal.Controllers
{
    public class HomeController : Controller
    {
        public async Task<IActionResult> Index()
        {
            // Verifica se a API tem recursos para atender a requisição
            if (Program.CheckIfThereIsThreadAvailable())
            {
                var httpClient = new HttpClient();
                // Chamada assincrona à API
                var dateTimeNow = await httpClient.GetStringAsync(Program.API_ADDRESS);
                // Calcula e gera a chave   
                var key = GenerateKey(dateTimeNow);
                // Aplica valores para View
                ViewData["DateTimeNow"] = dateTimeNow;
                ViewData["Key"] = key.Value;
                // Obtem soma dos números ímpares gerados na chave
                ViewData["Sum"] = key.SumOfOddNumbers;
                ViewData["VirtualMachines"] = Program.NUMBER_OF_VIRTUAL_MACHINES;
                // Aplica informações de memória para View
                ViewData["gc0"] = GC.CollectionCount(0);
                ViewData["gc1"] = GC.CollectionCount(1);
                ViewData["gc2"] = GC.CollectionCount(2);
                ViewData["currentMemory"] = ByteSize.FromBytes(GC.GetTotalMemory(false)).ToString();
                ViewData["privateBytes"] = ByteSize.FromBytes(Process.GetCurrentProcess().WorkingSet64);
            }
            else
                return BadRequest("Messages.THREAD_IS_NO_THREAD_AVAILABLE");

            // Retorna View
            return View();
        }

        private Key GenerateKey(string dateTimeNowString)
        {
            var random = new Random();
            var day = DateTime.Parse(dateTimeNowString).Day;
            var keyValue = string.Empty;            
            var keySumOfOddDigits = 0;
            for (int i = 0; i < 4096; i++)
            {
                var keyPart = day * i * random.Next(100, 9999);
                keySumOfOddDigits += GetSumOfOddDigitsFromNumber(keyPart);
                keyValue = string.Concat(keyValue, keyPart);
            }

            return new Key(keyValue, keySumOfOddDigits);
        }

        private int GetSumOfOddDigitsFromNumber(int number)
        {
            var sumOfOddDigits = 0;
            while (number != 0)
            {
                var currentDigit = number % 10;
                sumOfOddDigits += currentDigit % 2 == 1 ? currentDigit: 0;
                number /= 10;
            }

            return sumOfOddDigits;
        }


        public IActionResult Information()
        {
            return View();
        }

        private class Key {
            public string Value {get; private set;}
            public int SumOfOddNumbers {get; private set;}

            public Key(string value, int sumOfOddNumbers) {
                Value = value;
                SumOfOddNumbers = sumOfOddNumbers;
            }
        }
    }
}